require('dotenv').config()
var express =   require("express");
var fileupload = require('express-fileupload');
const AWS = require('aws-sdk')
const axios = require('axios')
const NodeClam = require('clamscan');
const fs = require('fs')

const ClamScan = new NodeClam().init({
  remove_infected: false, 
  quarantine_infected: false, 
  scan_log: null,
  debug_mode: false, 
  file_list: './downloads/', 
  scan_recursively: true, 
  clamscan: {
      path: '/usr/bin/clamscan', 
      db: null, 
      scan_archives: true, 
      active: true 
  },
  clamdscan: {
      socket: false, 
      host: false, 
      port: false, 
      timeout: 60000, 
      local_fallback: false, 
      path: '/usr/bin/clamdscan', 
      config_file: null, 
      multiscan: true, 
      reload_db: false, 
      active: true, 
      bypass_test: false, 
  },
  preference: 'clamdscan' 
});

const BUCKET_NAME ='uploadfileprototype'

const s3 = new AWS.S3({
    region: process.env.AWS_REGION,
    accessKeyId: process.env.accessKeyId,
    secretAccessKey: process.env.secretAccessKey,
    signatureVersion: 'v4'
})
const app = express()

app.use(fileupload({createParentPath: true}));

var data = []

app.post('/upload/newFile', async function(req,res){
  if(!req.files) res.send({message:'No Files'})
  else {
    await processFile(req,res)
  }
})


async function getSignedURLPut(file){
  return new Promise((resolve)=>{
    const url = s3.getSignedUrl('putObject', {
        Bucket: BUCKET_NAME,
        Key: file.name,
        Expires: 60*5,
        ContentType: file.mimetype
    });
    resolve(url)
  })
}

async function getSignedURLGet(file){
  return new Promise((resolve)=>{
    const url = s3.getSignedUrl('getObject', {
      Bucket: BUCKET_NAME,
      Key: file.name,
      Expires: 60*5
    });
  resolve(url)
  })
}

async function processFile(req,res){
  if(!req.files){
      res.send({
          message: "No File Selected",
          signedURL: signedUrl
  })
  } else {
    const fileArr = req.files.filename;
    if(fileArr.length > 1){
      for(file of fileArr){
        data = await uploadFile(req,res, file)
      }
    } else {
      data = await uploadFile(req,res, fileArr)
    }
  }
  res.send({
    message: 'Completed',
    data: data
  })
  data = []
}

async function uploadFile(req,res, file){
return new Promise(async(resolve)=>{
  let url = await getSignedURLPut(file)
  const options = { headers: { 'Content-Type': file.mimetype},maxContentLength: Infinity, maxBodyLength: Infinity };
  try{
      await axios.put(url, file.data, options) 
      var taggingStatus = await scanFile(res, file)
      data.push({
        name: file.name,
        uploadStatus: 'Success',
        TaggingStatus: taggingStatus
      })
      resolve(data)
  } catch(err){
      res.send({
          message: err
      })
  }
})
}

async function scanFile(res, scanfile){
  return new Promise((resolve)=>{
    var scanStatus = null;
    ClamScan.then(async clamscan => {
      try{
        var fileURL = await getSignedURLGet(scanfile)
        const getResp = await axios.get(fileURL,{responseType: 'arraybuffer',maxContentLength: 'Infinity', maxBodyLength: 'Infinity'})
        fs.writeFileSync("./downloads/"+scanfile.name, getResp.data);
        const { is_infected, viruses } = await clamscan.is_infected('./downloads/'+scanfile.name)
        if (is_infected) {
          console.log(scanfile.name + `is infected with ${viruses}!`);
          scanStatus = 'Infected'
        } 
        else {
          console.log(scanfile.name +' is SAFE')
          scanStatus = 'Safe'
        } 
        fs.unlinkSync('./downloads/'+scanfile.name)
        let taggingStatus = await setScanStatus(scanfile, scanStatus)
        resolve(taggingStatus)
      }catch(err){
        res.send({message:'Scanning error', err:err})
      }
    }).catch(err=>{
      console.log(err)
    })
  })
}

async function setScanStatus(file, status){
  return new Promise((resolve,reject)=>{
    var params = {
      Bucket: BUCKET_NAME, 
      Key: file.name, 
      Tagging: {
       TagSet: [
          {
         Key: "ScanStatus", 
         Value: status
        }
       ]
      }
     };
     s3.putObjectTagging(params, (err, res)=>{
       if(err) {
        console.log('Tagging error ', err)
         reject('Error')
       } 
       else {
        console.log('Tagging done')
         resolve('Successful')
       }
    })
  })
}

app.listen(3000,function(){
    console.log("Working on port 3000");
});